import { StyleSheet, Text,Button, View } from 'react-native';
import React, { useState, useEffect } from 'react';
import { SafeAreaView } from 'react-native-safe-area-context';
import MainNavigator from './src/navigation';



export default function App() {
  return (
    <SafeAreaView style={styles.SafeAreaView}>
      <MainNavigator/>
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  SafeAreaView: {
    flex: 1,
  },
});
