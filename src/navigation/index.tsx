import { NavigationContainer } from "@react-navigation/native";
import React from "react";
import IProducts from "../types/dtos/IProducts";
import { Routes } from "./constants";
import MainStack from "./MainStack";

export type RootStackParamList = {
[Routes.PRODUCT]: {product : IProducts}
[Routes.SCAN]: undefined;
};

const MainNavigator = () => {
  return (
    <NavigationContainer>
      <MainStack />
    </NavigationContainer>
  );
};

export default MainNavigator;
