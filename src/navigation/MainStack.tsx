import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import React from "react";
import { RootStackParamList } from ".";
import Product from "../components/Product";
import Scan from "../components/Scan";
import { Routes } from "./constants";
import { FontAwesome } from "@expo/vector-icons";

const { Navigator: Stack, Screen } =
  createBottomTabNavigator<RootStackParamList>();

const MainStack = () => {
  return (
    <Stack>
      <Screen
        options={{
          tabBarIcon: () => (
            <FontAwesome name="barcode" size={24} color="black" />
          ),
          tabBarLabel: "Scan",
        }}
        name={Routes.SCAN}
        component={Scan}
      />
      <Screen
        options={{
          tabBarIcon: ({ focused }) => (
            <FontAwesome name="product-hunt" size={24} color="black" />
          ),
          tabBarLabel: "Product",
        }}
        name={Routes.PRODUCT}
        component={Product}
      />
    </Stack>
  );
};

export default MainStack;
