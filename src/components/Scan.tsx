import { useNavigation } from '@react-navigation/core';
import { BarCodeEvent, BarCodeScanner } from 'expo-barcode-scanner';
import React, { useState, useEffect } from 'react';
import { StyleSheet, Text,Button, View } from 'react-native';
import { Routes } from '../navigation/constants';
import { getProduct } from '../services/ProductsService';

function Scan() {
    const [hasPermission, setHasPermission] = useState<boolean|null>(null);
    const [scanned, setScanned] = useState(false);
    const {navigate} = useNavigation();
  
    useEffect(() => {
      (async () => {
        const { status } = await BarCodeScanner.requestPermissionsAsync();
        setHasPermission(status === 'granted');
      })();
    }, []);
  
    const handleBarCodeScanned = async({ type, data }: BarCodeEvent) => {
      setScanned(true);
      const product = await getProduct(data);
      navigate(Routes.PRODUCT, {product});
    };
  
    if (hasPermission === null) {
      return <Text>Requesting for camera permission</Text>;
    }
    if (hasPermission === false) {
      return <Text>No access to camera</Text>;
    }
    return (
      <View style={styles.container}>
        <BarCodeScanner
          onBarCodeScanned={scanned ? undefined : handleBarCodeScanned}
          style={StyleSheet.absoluteFillObject}
        />
        {scanned && <Button title={'Tap to Scan Again'} onPress={() => setScanned(false)} />}
      </View>
    );
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
  });

export default Scan;

