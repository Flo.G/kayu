import { RouteProp, useRoute } from "@react-navigation/core";
import { useNavigation } from "@react-navigation/native";
import React from "react";
import {Text, View, Button} from 'react-native';
import { RootStackParamList } from "../navigation";
import { Routes } from "../navigation/constants";

type ProductScreenRouteProps = RouteProp<RootStackParamList, Routes.PRODUCT>

function Product() {
    const {params} = useRoute<ProductScreenRouteProps>();
    const {navigate} = useNavigation();
    return(
    <View>
        <Text>{params?.product?.product.product_name}</Text>
        <Text>{params?.product?.product.categories}</Text>
        <Text>{params?.product?.product.origins}</Text>
        <Button title="retour au scan " onPress={() => navigate(Routes.SCAN)}></Button>
    </View>
    )
}

export default Product;