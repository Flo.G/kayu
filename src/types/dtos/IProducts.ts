import { InteractionManagerStatic } from "react-native";

interface IProducts {
    code: string
    product:{
    categories: string
    image_small_url: string
    ingredients_text: string
    nutriscore_grade: string
    origins: string
    product_name: string
  }
}

export default IProducts;
  