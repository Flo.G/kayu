import axios from "axios";
import IProducts from "../types/dtos/IProducts";

export const getProduct = async (barecode: string) => {

  const response = await axios.get<IProducts>(
    `https://world.openfoodfacts.org/api/v0/product/${barecode}.json`
  );
  return response.data;
};